''' A custom project plugin for loading TOML frontmatter '''

from toml import loads

from shocking.plugin.base import BasePlugin


class TomlFrontmatterPlugin(BasePlugin):
    def process(self, file):
        content = file.metadata['content']

        if content.startswith('+++\n'):
            try:
                end_index = content.index('+++\n', 4)
            except ValueError:
                self._logger.warning('No closing found for frontmatter in "{}", skipping', file.source)

                return file

            frontmatter, stripped_content = content[4:end_index], content[end_index + 4:].strip()

            parsed_frontmatter = loads(frontmatter)
        
            return file._replace(
                metadata=file.metadata.update(content=stripped_content, **parsed_frontmatter))
        else:
            self._logger.warning('No frontmatter found for "{}"', file.source)

            return file