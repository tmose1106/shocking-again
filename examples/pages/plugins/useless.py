''' A custom project plugin that does nothing, used for testing '''

from shocking.plugin.base import BasePlugin


class UselessPlugin(BasePlugin):
    def process_all(self, files):
        return files