''' A custom project plugin for rendering an HTML document using a Jinja template '''

from pathlib import Path

from jinja2 import Environment, FileSystemLoader, Template

from shocking.plugin.base import BasePlugin


class JinjaFragmentPlugin(BasePlugin):
    ''' Render a fragment directly from a file using collection and file metadata '''
    def __init__(self, context, logger):
        super().__init__(context, logger)

        template_path = context.source.joinpath('_templates')

        self._env = Environment(
            loader=FileSystemLoader(template_path)
        )

        self._collection_metadata = context.metadata

    def process(self, file):
        output_path = file.output.with_name(file.output.stem)

        self._logger.debug('Rendering "{}" from file content', output_path)

        template = self._env.from_string(file.metadata['content'])

        return file._replace(
            output=output_path,
            metadata=file.metadata.set('content', template.render(file=file.metadata, collection=self._collection_metadata)))


class JinjaDocumentPlugin(BasePlugin):
    def __init__(self, context, logger):
        super().__init__(context, logger)

        template_path = context.source.joinpath('_templates')

        self._template_path = template_path

        self._env = Environment(
            loader=FileSystemLoader(template_path)
        )

        self._collection_metadata = context.metadata

        self._template = self._env.get_template(self._context.config['template'])

    def process(self, file):
        self._logger.debug('Rendering "{}" using template "{}"', file.output, self._template_path.joinpath(self._context.config['template']))

        return file._replace(
            metadata=file.metadata.set('content', self._template.render(file=file.metadata, collection=self._collection_metadata)))
