from csv import DictReader

from shocking.plugin.base import BasePlugin


class CsvContentPlugin(BasePlugin):
    ''' Read content of CSV file into collection metadata '''
    def process(self, file):
        lines = file.metadata['content'].split('\n')

        reader = DictReader(lines)

        items = list(reader)

        key = file.output.stem

        self._context.metadata[key] = items

        self._logger.debug('Copied contents of "{}" to metadata key "{}"', file.source, key)

        return file
