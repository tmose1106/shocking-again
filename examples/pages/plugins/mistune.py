''' A custom project plugin for rendering markdown fragments '''

from mistune import html

from shocking.plugin.base import BasePlugin


class MistunePlugin(BasePlugin):
    def process(self, file):
        new_content = html(file.metadata['content'])

        return file._replace(
            output=file.output.with_suffix('.html'),
            metadata=file.metadata.set('content', new_content))