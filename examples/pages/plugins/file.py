''' Generic plugins which handle working with files '''

from collections import namedtuple
from shutil import copyfile

from immutables import Map

from shocking.plugin.base import BasePlugin

File = namedtuple('File', ['source', 'output', 'content', 'metadata'])


def get_destination_file_path(source_dir_path, destination_dir_path, file_path):
    '''
    Get the file path as child of the destination path using its relative
    path to the source path.
    '''
    return destination_dir_path.joinpath(file_path.relative_to(source_dir_path))


def get_file_paths_from_file_string_list(source_dir_path, file_path_strings, logger):
    '''
    Parse list of file path strings as pathlib Path objects by interpreting
    the string as a glob or as a direct file path found in the source directory.

    Also a little messy, but pass in a logger for informative output.
    '''
    file_paths = []
    
    for file_string in file_path_strings:
        if '*' in file_string:        
            for file_path in source_dir_path.glob(file_string):
                logger.debug('File path glob "{}" found file path "{}"', file_string, file_path)

                file_paths.append(file_path)
        else:
            file_path = source_dir_path.joinpath(file_string)

            if file_path.is_file():
                logger.debug('File at path "{}" found', file_path)

                file_paths.append(file_path)

    return file_paths


class FileCopyPlugin(BasePlugin):
    def __init__(self, context, logger):
        super().__init__(context, logger)

        self._file_path_strings = context.config.get('files', [])

    def _process_path(self, file_path):
        if not file_path.is_file():
            self._logger.warning('File path "{}" does not exist, skipping')

        output_file_path = get_destination_file_path(self._context.source, self._context.output, file_path)

        if not output_file_path.parent.is_dir():
            output_file_path.parent.mkdir(parents=True)

        copyfile(file_path, output_file_path)

        self._logger.info('Copied "{}" to "{}"', file_path, output_file_path)

        return output_file_path

    def process_all(self, _):
        return [
            File(file_path, self._process_path(file_path), None, None)
            for file_path in get_file_paths_from_file_string_list(self._context.source, self._file_path_strings, self._logger)
        ]


class FileReadPlugin(BasePlugin):
    def __init__(self, context, logger):
        super().__init__(context, logger)

        self._collection_path_list = get_file_paths_from_file_string_list(context.source, context.config.get('files', []), self._logger)

    def process_all(self, _):
        file_tuples = [
            File(path, get_destination_file_path(self._context.source, self._context.output, path), '', Map({'content': path.read_text()}))
            for path in self._collection_path_list
        ]

        if len(file_tuples) == 0:
            self._logger.warning('File path list "{}" yielding no files', self._collection_path_list)

        return file_tuples


class FileWritePlugin(BasePlugin):
    def process(self, file):
        if not file.output.parent.is_dir():
            file.output.parent.mkdir(parents=True)

        file.output.write_text(file.metadata.get('content', ''))

        self._logger.info('Wrote processed content from "{}" to "{}"', file.source, file.output)

        return file
