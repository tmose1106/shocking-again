from logging import Formatter, LoggerAdapter, INFO

from click import style


COLOR_LEVELNAMES = {
    'CRITICAL': style("CRITICAL", fg="bright_red"),
    'DEBUG': style("DEBUG", fg="blue"),
    'ERROR': style("ERROR", fg="red"),
    'INFO': style("INFO", fg="green"),
    'WARNING': style("WARNING", fg="yellow"),
}


class BraceMessage:
    ''' Simple class from Python logging cookbook to use brace formatting in logs '''
    def __init__(self, fmt, /, *args, **kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.fmt.format(*self.args, **self.kwargs)


class BraceLogAdapter(LoggerAdapter):
    def __init__(self, logger, extra=None):
        super().__init__(logger, extra or {})

    def log(self, level, msg, /, *args, **kwargs):
        if self.isEnabledFor(level):
            msg, kwargs = self.process(msg, kwargs)
            self.logger._log(level, BraceMessage(msg, *args), (), **kwargs)


class ColoredLevelFormatter(Formatter):
    def format(self, record):
        return '{}: {}'.format(COLOR_LEVELNAMES[record.levelname], record.getMessage())
