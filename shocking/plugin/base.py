''' The base Shocking plugin '''

class BasePlugin:
    def __init__(self, context, logger):
        self._context = context
        self._logger = logger

    def process(self, file):
        raise NotImplementedError('Subclass must process files')

    def process_all(self, files):
        return [self.process(file) for file in files if file is not None]