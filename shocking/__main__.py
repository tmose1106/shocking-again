import asyncio
from functools import partial
from logging import getLogger, Formatter, StreamHandler, DEBUG, INFO
from pathlib import Path
from sys import exit as sys_exit

import click
from watchgod import awatch, Change

from . import ShockingCore
from .log import BraceLogAdapter, ColoredLevelFormatter


@click.group()
def cli():
    ''' A shockingly powerful static site generator '''
    pass


@cli.command()
@click.argument('project_path', type=Path)
@click.option('--debug', is_flag=True)
def build(project_path, debug):
    logger = getLogger('shocking')

    handler = StreamHandler()

    if debug:
        logger.setLevel(DEBUG)
        handler.setFormatter(Formatter(fmt='{levelname}:{name}:{message}', style='{'))
    else:
        logger.setLevel(INFO)
        handler.setFormatter(ColoredLevelFormatter())

    logger.addHandler(handler)

    try:
        core = ShockingCore(project_path, logger)
    except RuntimeError as error:
        logger.error(str(error))
        sys_exit(1)

    core.build_all()


async def watcher_main(watch_path, callback):
    loop = asyncio.get_running_loop()

    async for changes in awatch(watch_path):
        for change_type, path_string in changes:
            if change_type == Change.modified:
                await loop.run_in_executor(None, partial(callback, Path(path_string)))


@cli.command()
@click.argument('project_path', type=Path)
def serve(project_path):
    logger = getLogger('shocking')

    handler = StreamHandler()

    handler.setFormatter(Formatter(fmt='{levelname}:{name}:{message}', style='{'))

    logger.addHandler(handler)
    logger.setLevel(DEBUG)

    try:
        core = ShockingCore(project_path, logger)
    except RuntimeError as error:
        logger.error(str(error))
        sys_exit(1)

    core.build_all()

    loop = asyncio.new_event_loop()

    asyncio.set_event_loop(loop)

    loop.create_task(watcher_main(project_path.joinpath('source'), core.build_file))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print('Have a nice day :)')


if __name__ == '__main__':
    cli()