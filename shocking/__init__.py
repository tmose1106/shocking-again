from collections import namedtuple
from functools import partial
from itertools import chain

from immutables import Map
from toml import load

from .log import BraceLogAdapter
from .plugin.base import BasePlugin


PluginContext = namedtuple('PluginContext', ['source', 'output', 'config', 'metadata'])


def load_config(path):
    ''' Load toml based config from a path '''
    raw_config = load(path)

    return raw_config['shocking']


def _load_project_plugin_module(name, path):
    ''' Load a module by path '''
    from importlib import machinery, util
    
    module_name = f'shocking.project_plugin.{name}'

    loader = machinery.SourceFileLoader(module_name, str(path))

    spec = util.spec_from_loader(module_name, loader)

    module = util.module_from_spec(spec)

    loader.exec_module(module)

    return module


def _load_project_plugins_from_module(module, suffix='Plugin'):
    ''' Retrieve subclasses of BasePlugin from a module '''
    from inspect import getmembers, isclass

    suffix_length = len(suffix)

    return {
        member_name[:-suffix_length]: member_object
        for member_name, member_object in getmembers(module)
        if member_name.endswith('Plugin') and issubclass(member_object, BasePlugin) and member_object is not BasePlugin
    }


def get_collection_pipeline(plugins, names):
    '''
    The pipeline is defined as a list of strings. Each string contains a plugin
    name, as well as an optional ':' followed by an alias for when multiple
    instances of a plugin wish to be used in a single pipeline.
    '''
    for name in names:
        if ':' in name:
            plugin_name, config_name = name.split(':')

            yield (config_name, plugins[plugin_name])
        else:
            yield (name, plugins[name])


class ShockingCore:
    def __init__(self, project_path, logger):
        if not project_path.is_dir():
            raise RuntimeError(f'Directory at "{project_path}" does not exist')

        self._path = project_path

        project_config_path = project_path.joinpath('shocking.toml')

        if not project_config_path.is_file():
            raise RuntimeError(f'Project config "{project_config_path}" does not exist')

        self._config = load_config(project_config_path)

        self._core_logger = BraceLogAdapter(logger.getChild('core'))

        self._plugins = self._load_plugins()

        self._plugin_logger = logger.getChild('plugin')

        self._source_path = project_path.joinpath(self._config['core']['source'])
        self._output_path = project_path.joinpath(self._config['core']['output'])

        self._source_path_map = {}

        self._output_file_map = {}

    def _load_plugins(self):
        project_plugin_path = self._path.joinpath('plugins')

        if not project_plugin_path.is_dir():
            self._core_logger.debug('Project plugin directory at "{}" does not exist', project_plugin_path)

        project_modules = {
            plugin_path: _load_project_plugin_module(plugin_path.stem, plugin_path)
            for plugin_path in project_plugin_path.glob('*.py')
        }

        project_plugin_maps = [_load_project_plugins_from_module(module) for module in project_modules.values()]

        for path, module, plugins in zip(project_modules.keys(), project_modules.values(), project_plugin_maps):
            self._core_logger.debug('Loaded project module "{}" from path "{}"', module.__name__, path)

            for plugin_name in plugins.keys():
                self._core_logger.debug('Loaded plugin "{}" from module "{}"', plugin_name, module.__name__)

        project_plugins = dict(chain.from_iterable(plugins.items() for plugins in project_plugin_maps))

        return {**project_plugins}

    def _build_collection(self, collection_config):
        collection_metadata = collection_config.get('metadata', {})

        plugins = [
            (name, class_(
                PluginContext(self._source_path, self._output_path, collection_config.get('plugin', {}).get(name, {}), collection_metadata),
                BraceLogAdapter(self._plugin_logger.getChild(name))))
            for name, class_ in get_collection_pipeline(self._plugins, collection_config.get('pipeline', []))
        ]

        files = []

        for plugin_name, plugin in plugins:
            files = plugin.process_all(files)

        return files

    def build_file(self, file_path):
        try:
            build_func = self._source_path_map[file_path]

            build_func()
        except KeyError:
            self._core_logger.debug('File "{}" is not part of a collection', file_path)

    def build_all(self):
        for collection_name, collection_config in self._config.get('collections', {}).items():
            files = self._build_collection(collection_config)

            collection_func = partial(self._build_collection, collection_config)

            self._output_file_map[collection_name] = [file.output for file in files]

            self._source_path_map.update({file.source: collection_func for file in files})

        # pprint(self._source_path_map)
